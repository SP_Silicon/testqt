This is a test for how QT GUIs can work with the Slugbotics ROV.


This simple GUI basically put all the terminals and cameras we had in last year's robot into one program with tabs.

To run the program, have QT Creator installed, and point the project to the .pro file in QT Creator.

Word of warning, installing qtermwidget and Opencv took a while to do and I'm too lazy to write out how exactly to do it, however, there are good tutorials online if you want to just search.

Let's hope things work better this year in GUI standpoint, this was just a quick test.
