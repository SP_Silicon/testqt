#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qtermwidget5/qtermwidget.h>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    setUpConsole(ui->local_term);
    setUpConsole(ui->ssh_term,"", "ssh pi@192.168.20.1");
    setUpConsole(ui->talker_term, "~/Documents/QT");
    setUpConsole(ui->listener_term, "~/Downloads");

    processor = new VideoProcessor();
    processor->moveToThread(new QThread(this));
    connect(processor->thread(),
            SIGNAL(started()),
            processor,
            SLOT(startVideo()));
    connect(processor->thread(),
            SIGNAL(finished()),
            processor,
            SLOT(deleteLater()));
    connect(processor,
            SIGNAL(inDisplay(QPixmap)),
            ui->label,
            SLOT(setPixmap(QPixmap)));

    processor->thread()->start();
}

MainWindow::~MainWindow()
{
    processor->stopVideo();
    processor->thread()->quit();
    processor->thread()->wait();
    delete ui;
}

void setUpConsole(QWidget *widget, QString dir, QString comm) {
    QTermWidget *console = new QTermWidget();

    console->setParent(widget);
    console->setColorScheme("BreezeModified");
    console->changeDir(dir);
    console->sendText(comm);
    console->clear();


    console->setScrollBarPosition(QTermWidget::ScrollBarRight);
}

void MainWindow::on_pushButton_clicked()
{
    if (ui->checkBox->isChecked()) {
        processor->checked = true;
    }
    else {
        processor->checked = false;
    }
}
