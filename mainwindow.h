#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include "videoprocessor.h"
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>


void setUpConsole(QWidget *widget, QString dir = "", QString comm = "");
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;

VideoProcessor *processor;
};

#endif // MAINWINDOW_H
