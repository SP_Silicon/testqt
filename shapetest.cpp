#include "mainwindow.h"
#include <QDirIterator>
#include <math.h>

void drawShape(cv::Mat frame, QString shapeName, int pos, int currShape);

void matchShapesFromImage(cv::Mat frame, QString shapePathFormat){


    QDirIterator iter(shapePathFormat, QStringList() << "*.png", QDir::Files);

    int pos = 20;
    int i = 0;
    int j = 255;

    while(iter.hasNext()) {
        pos += 100;
        int currShape = 0;
        iter.next();
        cv::Mat gref, gtpl;
        QString shapeName = iter.fileName();
        //cv::Mat shape = cv::imread(shapePathFormat.toStdString() + "tpl-img.png");

        cv::Mat shape = cv::imread(shapePathFormat.toStdString() + shapeName.toStdString());
        //cv::resize(frame,frame,cv::Size(),,2,cv::INTER_AREA);
        int height = shape.rows;
        int width = shape.cols;
        double threshold = 0.8;

        cv::cvtColor(frame, gref, cv::COLOR_RGB2GRAY);
        cv::cvtColor(shape, gtpl, cv::COLOR_RGB2GRAY);
        cv::Mat res;
        cv::matchTemplate(gref, gtpl, res, CV_TM_CCOEFF_NORMED);
        cv::threshold(res, res, 0.8, 1., CV_THRESH_TOZERO);
        while (true)
        {
            double minval, maxval;
            cv::Point minloc, maxloc;
            cv::minMaxLoc(res, &minval, &maxval, &minloc, &maxloc);
            if (maxval >= threshold)
            {

                currShape++;
                cv::rectangle(
                            frame,
                            maxloc,
                            cv::Point(maxloc.x + width, maxloc.y + height),
                            CV_RGB(0,255,0), 2
                            );
                cv::floodFill(res, maxloc, cv::Scalar(0), nullptr, cv::Scalar(.1), cv::Scalar(1.));
            }
            else
                break;
        }


        shapeName=shapeName.section(".",0,0);
        drawShape(frame,shapeName,pos,currShape);
        i+= 50;
        j-= 50;
    }
}


void drawShape(cv::Mat frame, QString shapeName, int pos, int currShape) {
    shapeName=shapeName.section(".",0,0);
    if(shapeName.toLower() == QString::fromStdString("1circle")) {
        cv::circle(frame, cv::Point(80,pos), 20, cv::Scalar(0,0,255),cv::FILLED,cv::LINE_AA);

    }
   else if(shapeName.toLower() == QString::fromStdString("2triangle")) {

        std::vector<cv::Point> points;
        points.push_back(cv::Point(60,pos+20));
        points.push_back(cv::Point(100,pos+20));
        points.push_back(cv::Point(80,pos-20));
        std::vector<std::vector<cv::Point> > fillContAll;
        fillContAll.push_back(points);
        cv::fillPoly(frame,fillContAll,cv::Scalar(0,0,255));

    }
   else if(shapeName.toLower() == QString::fromStdString("3line")) {
        cv::line(frame, cv::Point(60,pos), cv::Point(100,pos), cv::Scalar(0,0,255),3,cv::LINE_AA);
    }
    else if(shapeName.toLower() == QString::fromStdString("4square")) {
        cv::rectangle(frame, cv::Rect(60,pos-20, 40,40), cv::Scalar(0,0,255),cv::FILLED,cv::LINE_AA);
    }

    cv::putText(frame,std::to_string(currShape),cv::Point(20,pos+10),cv::FONT_HERSHEY_SIMPLEX,1,cv::Scalar(0,0,255),2,cv::LINE_AA);
}

