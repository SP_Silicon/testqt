#include "videoprocessor.h"





VideoProcessor::VideoProcessor(QObject *parent) : QObject(parent)
{

}

void VideoProcessor::startVideo()
{
    QString processedShapes = QDir::currentPath() + "/processedShapes/";
    cv::VideoCapture cap(0);
    cv::Mat frame;
    stopped = false;
    //cap.release();
    while(cap.isOpened() && !stopped)
    {
        cap >> frame;
        if(frame.empty())
            continue;

        cv::flip(frame,frame, 1);
        if(checked)
            matchShapesFromImage(frame, processedShapes);


        emit inDisplay(
                    QPixmap::fromImage(
                        QImage(
                            frame.data,
                            frame.cols,
                            frame.rows,
                            frame.step,
                            QImage::Format_RGB888)
                        .rgbSwapped()));


    }

}
void VideoProcessor::stopVideo()
{
    stopped = true;
}
