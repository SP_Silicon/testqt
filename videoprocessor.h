#ifndef VIDEOPROCESSOR_H
#define VIDEOPROCESSOR_H

#include <QDir>
#include <QObject>
#include <QPixmap>
#include <QDebug>
#include <QMutex>
#include <QReadWriteLock>
#include <QSemaphore>
#include <QWaitCondition>
#include "opencv2/opencv.hpp"

void matchShapesFromImage(cv::Mat frame, QString shapePathFormat);
class VideoProcessor : public QObject
{
    Q_OBJECT
public:
        bool checked = false;
    explicit VideoProcessor(QObject *parent = nullptr);

signals:
    void inDisplay(QPixmap pixmap);
public slots:
    void startVideo();
    void stopVideo();
private:
    bool stopped;
};

#endif // VIDEOPROCESSOR_H
